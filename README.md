# Unity-UI-Intro-Workshop

## For people who have already installed SmartGit, you can clone the project by following these steps:
Click "Clone" on this page and copy the url in the popup window.  
Open SmartGit, click Repository > Clone or press Ctrl+Shift+O.  
Paste the link ("https://bitbucket.org/jingtan/unity-ui-intro-workshop.git") in Repository URL.  
Specify a location on your machine where you would like to clone this to.  
Open the project file location.

## If you don't have Smartgit installed yet:
Go to https://bitbucket.org/jingtan/unity-ui-intro-workshop/downloads/  
Click download repository.  
Unzip it and open the unity project.  
Open Unity-UI-Project-Files > Assets > Scenes.  
DynamicUIScreen_Finished shows the finished UI project that we will be creating - you can use it as a reference.  
DynamicUIScreen_Start is the starting point for tomorrow's workshop.